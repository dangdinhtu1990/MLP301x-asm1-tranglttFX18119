# -*- coding: utf-8 -*-
"""
Created on Sun Aug  7 21:07:20 2022

@author: Admin
"""
import pandas as pd
import numpy as np
import statistics


filename = input("Enter a filename: ")
try:
    f = open("D:/Trang_folder/DA/Data Files/" + filename + ".txt", 'r')
    # lines = len(f.readlines())
    
    # print('Total Number of lines:', line2)
    print("Successfully opened" + filename + ".txt")
    row_valid = 0
    row_invalid_not_26 = 0
    row_invalid_not_Nstart = 0
    #the right answer
    answer_key = "B,A,D,D,C,B,D,A,C,C,D,B,A,B,A,C,B,D,A,C,A,A,B,D,D"
    answer_list = answer_key.split(",")
    score_list = []
    student_list = []
    
    for line in f.readlines():
        line_split = line.split(",")
        if len(line_split) == 26 and line_split[0].startswith("N") == True:
            row_valid += 1
            
            #tính tổng điểm của từng record
            score = 0
            for i in range(25):
                if line_split[i + 1] == answer_list[i]:
                    score += 4
                elif line_split[i + 1] == '':
                    score += 0
                elif line_split[i + 1] != answer_list[i]:
                    score -= 1
            
            score_list.append(score)
            student_list.append(line_split[0])
                
        elif len(line_split) == 26 and line_split[0].startswith("N") == False:
                row_invalid_not_Nstart += 1
                print('Invalid line of data: N# is invalid:' + str(line))
        elif len(line_split) != 26 and line_split[0].startswith("N") == False:
                row_invalid_not_Nstart += 1
                row_invalid_not_26 += 1
                print('Invalid line of data: N# is invalid &\
                      does not contain exactly 26 values:' + str(line))
        else:
            row_invalid_not_26 += 1
            print('Invalid line of data: does not contain exactly 26 values:'\
                  + str(line_split))
            
    print("**** REPORT ****")
    print('Total Number of lines:', len(f.readlines()))
    print("Total valid lines of data: "+str(row_valid))
    print("not 26 in lines: "+ str(row_invalid_not_26))
    print("Not startwith N: "+ str(row_invalid_not_Nstart))
    print("Mean (average) score: "+ str(round(sum(score_list)/len(score_list), 2)))
    print("Highest score: "+ str(max(score_list)))
    print("Range of scores: "+ str(max(score_list) - min(score_list)))
    print("Median score: "+ str(statistics.median(score_list)))
    
    #write txt file have list score
    with open("D:/Trang_folder/DA/grades_" + str(filename) + ".txt","w") as grade:
        grade.write("# this is what " + str(filename) + "_grades.txt should look like \n")
        for i in range(len(student_list)):
            grade.write(str(student_list[i]) + "," + str(score_list[i]) + "\n")
    
# grade.close()
    
          
    
except :
    print("File cannot be found.")

# f.clcose()



# cài ubutu,máy ảo để train model
# khóa meachine learning của andrew
# vs code